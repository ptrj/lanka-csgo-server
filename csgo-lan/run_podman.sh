#!/bin/bash

podman stop csgo && podman rm csgo
podman run -d \
        --userns=keep-id \
        --user=1000:1000 \
        --volume ./volumes/servercfg:/home/csgo/lgsm/serverfiles/csgo/cfg \
        -p "27015:27015/tcp" \
        -p "27015:27015/udp" \
        -p "27020:27020/udp" \
        -p "27031:27031/udp" \
        -p "27032:27032/udp" \
        -p "27033:27033/udp" \
        -p "27034:27034/udp" \
        -p "27035:27035/udp" \
        -p "27036:27036/udp" \
        -p "27036:27036/tcp" \
        --name csgo \
local/csgo:latest && \
podman logs -f csgo

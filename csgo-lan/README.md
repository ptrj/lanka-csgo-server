# Custom CS:GO Server
Approximate building time 20min, approximate image size 35GB
## Settings
Change game settings in `image/files/server.cfg` before build \
or after image build you can find it in `./volumes/servercfg/server.cfg`
## Run
```
docker-compose up -d
```
## Start/Stop/Restart
```
docker-compose start
docker-compose stop
docker-compose restart
```
## Remove
```
docker-compose down
docker rmi local/csgo
```

## CS:GO Commands
```
connect 192.168.1.100 - Connect to LAN server on IP 192.168.1.100
rcon_password foo - Where `foo` is rcon password sets in server.cfg
rcon mp_warmup_end - This command ends the warmup
rcon mp_restartgame 10 - This command will restart the game. \
If you specify a number as a parameter, this amount of seconds will be counted down before the restart happens.
rcon changelevel de_dust2 - This command is used to change the map you're playing on.
```
### Bots
```
rcon bot_quota_mode normal (or competitive) https://totalcsgo.com/command/botquotamode
rcon bot_add
rcon bot_add_t
rcon bot_add_ct
rcon bot_kick
```
More: https://totalcsgo.com/commands

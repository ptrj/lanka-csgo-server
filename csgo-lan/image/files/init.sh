#!/bin/bash

cp -a $HOME/lgsm/serverfiles/csgo/cfg $HOME/lgsm/serverfiles/csgo/cfg_copy
sed -i 's/mp_ct_default_secondary.*/mp_ct_default_secondary weapon_usp_silencer/' $HOME/lgsm/serverfiles/csgo/cfg_copy/gamemode_competitive.cfg
echo "mp_autoteambalance 0" >> $HOME/lgsm/serverfiles/csgo/cfg_copy/gamemode_competitive.cfg

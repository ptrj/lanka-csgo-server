#!/bin/bash

if [ ! -f $HOME/.firstrun ]
then
    test "$(ls -A $HOME/lgsm/serverfiles/csgo/cfg | grep -v .empty)" || cp -a $HOME/lgsm/serverfiles/csgo/cfg_copy/. $HOME/lgsm/serverfiles/csgo/cfg
    touch $HOME/.firstrun
fi

./lgsm/csgoserver update
./lgsm/csgoserver start && tail -f ./lgsm/log/console/*

#!/bin/bash

if [ ! -f /home/mc/lgsm/serverfiles/.firstrun ]
then
    test "$(ls -A /home/mc/lgsm/serverfiles | grep -v .keep)" || cp -a /home/mc/lgsm/serverfiles_copy/. /home/mc/lgsm/serverfiles
    touch /home/mc/lgsm/serverfiles/.firstrun
fi

./lgsm/mcbserver update
./lgsm/mcbserver start && tail -f ./lgsm/log/console/*
